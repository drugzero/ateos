// Type definitions for ateos 0.7
// Project: https://gitlab.com/taaliman/ateos
// Definitions by: Taaliman <https://gitlab.com/taaliman>
// Definitions: https://github.com/DefinitelyTyped/DefinitelyTyped
// TypeScript Version: 2.4

/// <reference path="./ateos.d.ts" />
/// <reference path="./glosses/app.d.ts" />
/*
/// <reference path="./glosses/archives.d.ts" />
/// <reference path="./glosses/assertion.d.ts" />
/// <reference path="./glosses/collections/index.d.ts" />
/// <reference path="./glosses/compressors.d.ts" />
/// <reference path="./glosses/crypto/index.d.ts" />
/// <reference path="./glosses/data.d.ts" />
/// <reference path="./glosses/datetime.d.ts" />
/// <reference path="./glosses/events.d.ts" />
/// <reference path="./glosses/errors.d.ts" />
/// <reference path="./glosses/fake.d.ts" />
/// <reference path="./glosses/fast.d.ts" />
/// <reference path="./glosses/fs.d.ts" />
/// <reference path="./glosses/is.d.ts" />
/// <reference path="./glosses/math/index.d.ts" />
/// <reference path="./glosses/meta.d.ts" />
/// <reference path="./glosses/net/index.d.ts" />
/// <reference path="./glosses/pretty/index.d.ts" />
/// <reference path="./glosses/promise.d.ts" />
/// <reference path="./glosses/regex.d.ts" />
/// <reference path="./glosses/semver.d.ts" />
/// <reference path="./glosses/shani-global.d.ts" />
/// <reference path="./glosses/shani.d.ts" />
/// <reference path="./glosses/std.d.ts" />
/// <reference path="./glosses/streams.d.ts" />
/// <reference path="./glosses/system/index.d.ts" />
/// <reference path="./glosses/templating/index.d.ts" />
/// <reference path="./glosses/text/index.d.ts" />
/// <reference path="./glosses/utils.d.ts" />
/// <reference path="./glosses/vault.d.ts" />
*/
declare const _ateos: typeof ateos;

export { _ateos as ateos };
