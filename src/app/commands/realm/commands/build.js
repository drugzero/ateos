const {
    app: { Subsystem, mainCommand },
    cli
} = ateos;

export default class extends Subsystem {
    @mainCommand({
        arguments: [
            {
                name: "path",
                nargs: "?",
                help: "Project entry path"
            }
        ],
        options: [
            {
                name: ["-re", "--re"],
                help: "Interpret 'path' as regular expression"
            }
        ]
    })
    async build(args, opts) {
        try {
            // cli.updateProgress({
            //     message: `building ${cli.theme.primary(args.has("path") ? args.get("path") : "whole project")}`
            // });

            const path = this.parent.resolvePath(args, opts);
            const r = await this.parent.connectRealm({
                cwd: process.cwd(),
                progress: false
            });
            await r.runAndWait("build", {
                path
            });

            // cli.updateProgress({
            //     message: "done",
            //     // clean: true,
            //     status: true
            // });
            return 0;
        } catch (err) {
            // cli.updateProgress({
            //     message: err.message,
            //     status: false,
            //     clean: true
            // });
            ateos.log.bright.red.error.noLocate(err);
            return 1;
        }
    }
}
