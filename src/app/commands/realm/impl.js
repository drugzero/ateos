const {
    app,
    is,
    cli,
    realm
} = ateos;

const {
    subsystem
} = app;

const subCommand = (name) => ateos.path.join(__dirname, "commands", name);

@subsystem({
    commandsGroups: [
        {
            name: "own",
            description: "ATEOS specific"
        },
        {
            name: "generic",
            description: "Generic commands"
        }
    ],
    subsystems: [
        {
            name: "build",
            group: "generic",
            description: "Build realm sources",
            subsystem: subCommand("build")
        },
        {
            name: "clean",
            group: "generic",
            description: "Clean realm build files",
            subsystem: subCommand("clean")
        },
        {
            name: ["create", "new"],
            group: "generic",
            description: "Create new realm",
            subsystem: subCommand("create")
        },
        {
            name: "dev",
            group: "generic",
            description: "Start realm development cycle",
            subsystem: subCommand("dev")
        },
        {
            name: "fork",
            group: "generic",
            description: "Fork realm",
            subsystem: subCommand("fork")
        },
        {
            name: "info",
            group: "generic",
            description: "Show realm information",
            subsystem: subCommand("info")
        },
        {
            name: "link",
            group: "generic",
            description: "Create/delete [sym]links to realm",
            subsystem: subCommand("link")
        },
        {
            name: "merge",
            group: "own",
            description: "Merge realm into ATEOS",
            subsystem: subCommand("merge")
        },
        {
            name: "pack",
            group: "generic",
            description: "Pack realm into the archive",
            subsystem: subCommand("pack")
        },
        {
            name: "publish",
            group: "generic",
            description: "Publish realm release",
            subsystem: subCommand("publish")
        }
    ]
})
export default () => class RealmCommand extends app.Subsystem {
    resolvePath(args, opts) {
        let path = args.has("path") ? args.get("path") : null;
        if (is.string(path) && opts.has("re")) {
            path = new RegExp(path);
        }
        return path;
    }

    async connectRealm({ cwd, progress = false } = {}) {
        let manager;
        if (is.string(cwd)) {
            manager = new realm.RealmManager({ cwd });
        } else {
            manager = realm.rootRealm;
        }
        await manager.connect({
            transpile: true
        });
        progress && await cli.observe("progress", manager);
        return manager;
    }
};
