ateos.lazify({
    MemoryDatastore: "./memory",
    FsDatastore: "./fs",
    LevelDatastore: "./level",
    PubsubDatastore: "./pubsub"
}, exports, require);
