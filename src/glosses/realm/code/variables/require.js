export default class RequireVariable extends ateos.realm.code.Variable {
    constructor() {
        super({
            name: "require"
        });
    }
}
