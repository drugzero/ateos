export default class XImport extends ateos.realm.code.Base {
    getType() {
        return "ImportDeclaration";
    }
}
