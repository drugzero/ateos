ateos.lazify({
    Emitter: "eventemitter3",
    AsyncEmitter: "./async_emitter"
}, ateos.asNamespace(exports), require);
