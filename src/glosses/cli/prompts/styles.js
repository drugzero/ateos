const utils = require("./utils");

const {
    is,
    cli: { style }
} = ateos;

const styles = {
    default: style.noop,
    noop: style.noop,

    /**
   * Modifiers
   */

    set inverse(custom) {
        this._inverse = custom;
    },
    get inverse() {
        return this._inverse || utils.inverse(this.primary);
    },

    set complement(custom) {
        this._complement = custom;
    },
    get complement() {
        return this._complement || utils.complement(this.primary);
    },

    /**
   * Main color
   */

    primary: style.cyan,

    /**
   * Main palette
   */

    success: style.green,
    danger: style.magenta,
    strong: style.bold,
    warning: style.yellow,
    muted: style.dim,
    disabled: style.gray,
    dark: style.dim.gray,
    underline: style.underline,

    set info(custom) {
        this._info = custom;
    },
    get info() {
        return this._info || this.primary;
    },

    set em(custom) {
        this._em = custom;
    },
    get em() {
        return this._em || this.primary.underline;
    },

    set heading(custom) {
        this._heading = custom;
    },
    get heading() {
        return this._heading || this.muted.underline;
    },

    /**
   * Statuses
   */

    set pending(custom) {
        this._pending = custom;
    },
    get pending() {
        return this._pending || this.primary;
    },

    set submitted(custom) {
        this._submitted = custom;
    },
    get submitted() {
        return this._submitted || this.success;
    },

    set cancelled(custom) {
        this._cancelled = custom;
    },
    get cancelled() {
        return this._cancelled || this.danger;
    },

    /**
   * Special styling
   */

    set typing(custom) {
        this._typing = custom;
    },
    get typing() {
        return this._typing || this.dim;
    },

    set placeholder(custom) {
        this._placeholder = custom;
    },
    get placeholder() {
        return this._placeholder || this.primary.dim;
    },

    set highlight(custom) {
        this._highlight = custom;
    },
    get highlight() {
        return this._highlight || this.inverse;
    }
};

styles.merge = (options = {}) => {
    if (options.styles && is.boolean(options.styles.enabled)) {
        style.enabled = options.styles.enabled;
    }
    if (options.styles && is.boolean(options.styles.visible)) {
        style.visible = options.styles.visible;
    }

    const result = utils.merge({}, styles, options.styles);
    delete result.merge;

    for (const key of Object.keys(style)) {
        if (!result.hasOwnProperty(key)) {
            Reflect.defineProperty(result, key, { get: () => style[key] });
        }
    }

    for (const key of Object.keys(style.styles)) {
        if (!result.hasOwnProperty(key)) {
            Reflect.defineProperty(result, key, { get: () => style[key] });
        }
    }
    return result;
};

module.exports = styles;
