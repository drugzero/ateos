const _colors = require("colors");

export default {
    format: `${_colors.grey(" {bar}")} {percentage}% | ETA: {eta}s | {value}/{total}`,
    barCompleteChar: "\u2588",
    barIncompleteChar: "\u2591"
};
