ateos.lazify({
    legacy: "./legacy",
    shades_classic: "./shades-classic",
    shades_grey: "./shades-grey",
    rect: "./rect"
}, exports, require);

