ateos.lazify({
    // error: "./error",
    json: "./json",
    size: "./size",
    table: "./table",
    ms: "./ms",
    time: "./time",
    timeZone: "./time_zone"
}, ateos.asNamespace(exports), require);
